fun main(){

    val first = Point(1,2)

    val second = Point(1, 2)


    println(first.equals(second))

    println(first.toString())

    println(first.move(1,2))



//  for fraction class

    val f1 = Fraction(1,2)

    val f2 = Fraction(1,2)



    println(f1.getSum(3.5,4.3))

    println(f1.multiplication(5.0,5.0))

}


class Point{

     private var x: Int = 0
     private var y: Int = 0


    constructor(a:Int, b:Int){
        x = a
        y = b
    }

    // toString მეთოდი - რომელიც უნდა აბრუნებდეს კლასის ველებს String ფორმატში

    override fun toString(): String {
        return "${this.x} / ${this.y}"
    }

    //equals მეთოდი - უნდა იძლეოდეს საშუალებას შევადაროთ 2 point კლასის ობიექტი

    override fun equals(other: Any?): Boolean {
        if(other is Point){
            if(x * other.y == y * other.x){
                return true
            }
        }
        return false
    }

    //  მეთოდი, რომელებიც სათავის მიმართ სიმეტრიულად გადაიტანს წერტილს

    fun move(x:Int,y:Int):String{

        if(x <0 || x == 0){

            var y_1 = y - y - y

            return "x ღერძის მიმართ სიმეტრიულია მოცემულ წერტილში :  $x: $y_1"

        }else {

            return "x ღერძის მიმართ სიმეტრიულია მოცემულ წერტილში : $x: -$y"
        }
    }

}



// Fraction class methods


class Fraction(private var numerator: Int, private var denominator: Int){

    override fun equals(other: Any?): Boolean {
        if(other is Fraction){
            if(numerator * other.denominator == denominator * other.numerator)
                return true
        }
        return false
    }

    override fun toString(): String {
        return "${this.numerator} / ${this.denominator}"
    }


//    შეკრების მეთოდი

   fun getSum(first:Double, second: Double): Double {
        return first + second
    }

//   გამრავლების მეთოდი

    fun multiplication(first:Double, second: Double): Double {
       return first * second
    }

}




